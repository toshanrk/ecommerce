
<?php
require __DIR__ . '/vendor/autoload.php';

//Create instance of MongoDB client
$client = new MongoDB\Client("mongodb://localhost:27017");#

//Get name and adress strings
$name=filter_input(INPUT_POST,'name',FILTER_SANITIZE_STRING);
$password=filter_input(INPUT_POST,'password',FILTER_SANITIZE_STRING);
$email= filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);

// Select a database
$db = $client ->ecommerce;

// Select a collection
$collection = $db ->Customers;

//create a php array with our search criteria 
$findCriteria= ["name" =>$name ];

//Find all customer that match the criteria
$resultArray = $db ->Customers->find($findCriteria)->toArray();


//  Extract the data that was sent to the server
$name= filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
$email= filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
$password= filter_input(INPUT_POST,'password', FILTER_SANITIZE_STRING);

//Convert to php array
$dataArray = [
    "name"=>  $name,
    "email"=> $email,
    "password"=> $password
];

if(count($resultArray)>=1){
    echo 'Database error: Multiple accounts have the same username.';
    return;
}   

//Add the new customer to the database
$insertResult = $collection-> insertOne($dataArray);

//Echo result back to user
if($insertResult->getInsertedCount()==1){
    echo 'Customer added.';
    echo 'New document id: '.$insertResult->getInsertedId();
}
else{
    echo 'Error adding customer';
}
?>