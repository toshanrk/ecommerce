<?php
// function to output common header html code and linking css stylesheet file for all webpages
function output_header($title){
    echo '<!DOCTYPE html>';
    echo '<html>';
    echo '<head>';
    echo '<meta charset="UTF-8">';
    echo '<title>'.$title.'</title>';
    echo '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
    echo '<link rel="shortcut icon" href="Images/Galactech_logo.png">';
    echo '<link rel="stylesheet" href="CSS/style.css">';
    echo '<script src="https://kit.fontawesome.com/3225270ca2.js" crossorigin="anonymous"></script>';
    echo '</head>';
    echo '<body>';
}
function output_navigation(){
    echo '<header>';
    echo '<nav>';
    echo '<div class="navigation">';
    echo '<div class="logo"><a href="index.php"><img src="Images/Tech_company.gif" alt="Galactech logo"></a></div>';
    echo '<div class="options">';
    echo '<ul>';
    echo '<li><a href="index.php">Home</a></li>';
    echo '<li class="dropdown">Shop';
    echo '<i class="fas fa-chevron-down" style="padding: 0 0 0 8px; font-size: 10px"></i>';
    echo '<div class="dropdown-content">';
    echo '<div class="dropdowncontent">';
    echo '<div class="monitor"><a href="shop.php#section1">Computers & Laptops</a><br>';
    echo '<img src="Images/Alienware_25_monitor__25-inch_1080p_at_240Hz_with__Fast_IPS__panel_1-removebg-preview.png"
    width=70%>';
    echo '</div>';
    echo '<div class="acessories"><a href="shop.php#section3">Accessories</a><br>';
    echo '<img src="Images/SONY_Headset_1.png" width=35%>';
    echo '</div>';
    echo '<div class="mobile"><a href="shop.php#section2">Mobile & Tablets</a><br>';
    echo '<img src="Images/Iphone_1.png" width=50%>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</li>';
    echo '<a href="about-us.php"><li>About Us</li></a>';
    echo '</ul>';
    echo '</div>';
    echo '<div class="links">';
    echo '<ul>';
    echo '<li>';
    echo '<div class="search-wrapper">';
    echo '<span><i class="fas fa-search"></i>';
    echo '<input type="search" placeholder="Search here" /></span>';
    echo '</div>';
    echo '</li>';
    echo '<li class="user-status"><i id="user" class="fas fa-user"></i>';
    echo '<div class="user-status-content">';
    echo '<div class="user-status-dropdowncontent">';
    echo '<a id="signinbtn" href="register.php">Sign up</a>';
    echo '</div>';
    echo '</div>';
    echo '</li>';
    echo '<li class="shopping-cart"><i class="fas fa-shopping-cart"></i>';
    echo '<div class="shopping-cart-content">';
    echo '<div class="shopping-cart-dropdowncontent">';
    echo '<p>My Cart</p>';
    echo '<button class ="checkout" type="button"><a href="cart.php">Check out</a></button>';
    echo '</div>';
    echo '</ul>';
    echo '</div>';
    echo '</div>';
    echo '</nav>';
    echo '</header>';
}


function output_footer(){
    echo '<footer>';
    echo '<div class="footer">';
    echo '<div class="description">';
    echo '<p>Galactech is one of Mauritius most reputable online electronics retailers. Galactech began
    operations in 2022 as a small-scale electronics firm and has since grown to become one of Mauritius
    safest and most responsive electronics and technology stores.</p>';
    echo '<h4>FOLLOW US</h4>';
    echo '<span>';
    echo '<a href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a>';
    echo '<a href="https://www.facebook.com/"><i class="fab fa-facebook-square"></i></a>';
    echo '<a href="https://www.youtube.com/"><i class="fab fa-youtube-square"></i></a>';
    echo '</span>';
    echo '</div>';
    echo '<div class="info">';
    echo '<h3>Information</h3>';
    echo '<a href="about-us.php"><p>About us</p></a>';
    echo '<p>FAQ</p>';
    echo '<p>Contact us</p>';
    echo '<a href="staff-login.php"><h5>Staff log in</h5></a>';
    echo '</div>';
    echo '<div class="service">';
    echo '<h3>Customer service</h3>';
    echo '<p>Return policy</p>';
    echo '<p>Privacy policy</p>';
    echo '<p>Terms & Conditions</p>';
    echo '<p>Shipping policy</p>';
    echo '</div>';
    echo '<div class="contact">';
    echo '<h3>Contact</h3>';
    echo '<p>info@galactech.com</p>';
    echo '<p>+230 5463 6743</p>';
    echo '</div>';
    echo '</footer>';
    echo '</body>';
    echo '</html>';
}