<?php
    include 'common.php';
    output_header("Galactech");
?>

<?php
    output_navigation();
?>
<div class="body">
    <div class="container">
        <div class="forms-container">
            <div class="signin-signup">
            <form action="#" class="staff-login-form">
                <h2 class="title">Staff Log in</h2>
                <div class="input-field">
                    <i class="fas fa-user"></i>
                    <input type="text" placeholder="Username" id="name" name="name">
                </div>
                <div class="input-field">
                    <i class="fas fa-lock"></i>
                    <input type="password" placeholder="Password" id="password" name="password" />
                </div>
                <input type="submit" class="btn" value="Log in" />
            </form>
            </div>
            <div class="staff_description">
                <p>Log in to view, add, edit or delete orders and products.</p>
            </div>
            <img src="Images/employee.svg" class="employee_image"/>
            
        </div>
    </div>
</div>
    
<?php
output_footer()
?>

