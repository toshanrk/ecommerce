<?php
    include 'common.php';
    output_header("Galactech");
?>
<link rel="stylesheet" type="text/css" href="CSS/lightslider.css">
<script type="text/javascript" src="JS/Jquery.js"></script>
<script type="text/javascript" src="JS/lightslider.js"></script>
<script type="text/javascript" src="JS/script.js"></script>

<?php
    output_navigation();
?>
<div class="main">
    <div id="section1">
        <h2>Computers and Laptops</h2>
        <div class="slider-container">
        <ul id="autoWidth" class="cs-hidden">
            <li class="item-a">
                <div class="box">
                    <img class="model" src="Images/Alienware_25_monitor__25-inch_1080p_at_240Hz_with__Fast_IPS__panel_1-removebg-preview.png"/>
                    <div class="details">
                        <p>Alienware 25 monitor 25-inch 1080p at 240Hz with Fas IPS</p>
                        <p>Rs 20 000</p>
                    </div>
                    <button class="add-to-cart" name= "61e57bcf237dfb297db8719f" type="button">Add to cart <i class="fas fa-shopping-cart"></i></button>
                    
                </div>
            </li>
            <li class="item-b">
                <div class="box">
                    <img class="model" src="Images/Macbook_Pro__2021__1.png"/>
                    <div class="details">
                        <p>2021 Apple MacBook Pro (16-inch, 16GB RAM, 512GB SSD)</p>
                        <p>Rs 109 450</p>
                    </div>
                    <button class="add-to-cart" name= "61e5f0205a884ac2840f2c40" type="button">Add to cart <i class="fas fa-shopping-cart"></i></button>
                    
                </div>
            </li>
            <li class="item-c">
                <div class="box">
                    <img class="model" src="Images/HP_Spectre__2019__1.png"/>
                    <div class="details">
                        <p>2019 HP Spectre x360 15.6" 4K Ultra HD Touchscreen 2-in-1 Laptop Computer</p>
                        <p>Rs 89 745</p>
                    </div>
                    <button class="add-to-cart" name="61e5ebdd5a884ac2840f2c36" type="button">Add to cart <i class="fas fa-shopping-cart"></i></button>
                    
                </div>
            </li>
            <li class="item-d">
                <div class="box">
                    <img class="model" src="Images/Mouse_1.png"/>
                    <div class="details">
                        <p>Gaming mouse</p>
                        <p>Rs 3 500</p>
                    </div>
                    <button class="add-to-cart" name="" type="button">Add to cart <i class="fas fa-shopping-cart"></i></button>
                    
                </div>
            </li>
            <li class="item-e">
                <div class="box">
                    <img src="Images/Logitech_G815_keyboard_1.png" style="height: 95px; margin: 16%; padding-top: 10%;"/>
                    <div class="details">
                        <p>Logitech G815 LIGHTSYNC RGB Mechanical Gaming Keyboard</p>
                        <p>Rs 7 400</p>
                    </div>
                    <button class="add-to-cart" name="61e5ef9e5a884ac2840f2c3e" type="submit">Add to cart <i class="fas fa-shopping-cart"></i></button>
                    
                </div>
            </li>
        </ul>

    </div>
    <div id="section2">
        <h2>Mobiles and Tablets</h2>
        <div class="slider-container">
        <ul id="autoWidth2" class="cs-hidden2">
            <li class="item-1">
                <div class="box">
                    <img class="model" src="Images/Samsung_phone_1.png"/>
                    <div class="details">
                        <p>Samsung Galaxy Z Fold 2</p>
                        <p>Rs 43 580</p>
                    </div>
                    <button class="add-to-cart" name="61e5f0e35a884ac2840f2c42" type="button">Add to cart <i class="fas fa-shopping-cart"></i></button>
                    
                </div>
            </li>
            <li class="item-2">
                <div class="box">
                    <img class="model" src="Images/Iphone_1.png"/>
                    <div class="details">
                        <p>Iphone 13 Pro Max, 128 GN, Sierra Blue</p>
                        <p>Rs 63 500</p>
                    </div>
                    <button class="add-to-cart" name="61e5ee405a884ac2840f2c3c" type="button">Add to cart <i class="fas fa-shopping-cart"></i></button>
                    
                </div>
            </li>
            <li class="item-3">
                <div class="box">
                    <img class="model" src="Images/Huawei_1.png"/>
                    <div class="details">
                        <p>Huawei P30 Pro 128GB 8GB RAM</p>
                        <p>Rs 26 600</p>
                    </div>
                    <button class="add-to-cart" name="61e5ec8a5a884ac2840f2c38" type="button">Add to cart <i class="fas fa-shopping-cart"></i></button>
                    
                </div>
            </li>
            <li class="item-4">
                <div class="box">
                    <img class="model" src="Images/Wacom_Cintiq_graphic_tablet_1.png"/>
                    <div class="details">
                        <p>Wacom DTK1660K0A Cintiq 16 Drawing Tablet with Screen </p>
                        <p>Rs 28 400</p>
                    </div>
                    <button class="add-to-cart" name="61e5f3e85a884ac2840f2c48" type="button">Add to cart <i class="fas fa-shopping-cart"></i></button>
                    
                </div>
            </li>
            <li class="item-5">
                <div class="box">
                    <img class="model" src="Images/ipad_air__2020__1.png"/>
                    <div class="details">
                        <p>2020 Apple iPad Air (10.9-inch, Wi-Fi + Cellular, 64GB) Green (4th Generation)</p>
                        <p>Rs 29 950</p>
                    </div>
                    <button class="add-to-cart" name="61e5ed5c5a884ac2840f2c3a" type="button">Add to cart <i class="fas fa-shopping-cart"></i></button>
                    
                </div>
            </li>
        </ul>

    </div>
    <div id="section3">
        <h2>Acessories</h2>
        <div class="slider-container">
        <ul id="autoWidth3" class="cs-hidden3">
            <li class="item-1">
                <div class="box">
                    <img class="model" src="Images/SONY_Headset_1.png"/>
                    <div class="details">
                        <p>Sony Wireless Industry Leading Noise Canceling Overhead Headphones</p>
                        <p>Rs 15 200</p>
                    </div>
                    <button class="add-to-cart" name="61e5eae15a884ac2840f2c34" type="button">Add to cart <i class="fas fa-shopping-cart"></i></button>
                    
                </div>
            </li>
            <li class="item-2">
                <div class="box">
                    <img class="model" src="Images/Camera_stand_1.png"/>
                    <div class="details">
                        <p>Tripod Stand For DLSR Camera With Mobile Holder</p>
                        <p>Rs 2 700</p>
                    </div>
                    <button class="add-to-cart" name="61e5e8375a884ac2840f2c2b" type="button">Add to cart <i class="fas fa-shopping-cart"></i></button>
                    
                </div>
            </li>
            <li class="item-3">
                <div class="box">
                    <img class="model" src="Images/Earbuds_1.png"/>
                    <div class="details">
                        <p>Beats Studio Buds – True Wireless Noise Cancelling Earbuds</p>
                        <p>Rs 6 520</p>
                    </div>
                    <button class="add-to-cart" name="61e5e90e5a884ac2840f2c30" type="button">Add to cart <i class="fas fa-shopping-cart"></i></button>
                    
                </div>
            </li>
            <li class="item-4">
                <div class="box">
                    <img class="model" src="Images/Webcam_1.png"/>
                    <div class="details">
                        <p>Webcam with Microphone, 30FPS Full HD 1080P</p>
                        <p>Rs 2 180</p>
                    </div>
                    <button class="add-to-cart" name="61e5f4735a884ac2840f2c4a" type="button">Add to cart <i class="fas fa-shopping-cart"></i></button>
                    
                </div>
            </li>
            <li class="item-5">
                <div class="box">
                    <img class="model" src="Images/Speaker_1.png"/>
                    <div class="details">
                        <p>Bose SoundLink Color II: Portable Bluetooth, Wireless Speaker with Microphone</p>
                        <p>Rs 5 650</p>
                    </div>
                    <button class="add-to-cart" name="61e5f27c5a884ac2840f2c46" type="button">Add to cart <i class="fas fa-shopping-cart"></i></button>
                    
                </div>
            </li>
        </ul>

    </div>
</div>
<?php
output_footer()
?>