<?php
    include 'common.php';
    output_header("Register");
?>

<?php
    output_navigation();
?>
<div class="body">
    <div class="container">
        <div class="forms-container">
            <div class="signin-signup">
                <form action="login.php" class="sign-in-form" method="post">
                    <h2 class="title">Sign in</h2>
                    <div class="input-field">
                        <i class="fas fa-user"></i>
                        <input type="text" placeholder="Username" id="name" name="name" />
                    </div>
                    <div class="input-field">
                        <i class="fas fa-lock"></i>
                        <input type="password" placeholder="Password" id="password" name="password" />
                    </div>
                    <input type="submit" value="Login" class="btn solid" />
                </form>
                <form action="registration.php" class="sign-up-form" method="post">
                    <h2 class="title">Sign up</h2>
                        <div class="input-field">
                        <i class="fas fa-user"></i>
                        <input type="text" placeholder="Username" id="name" name="name" />
                    </div>
                    <div class="input-field">
                        <i class="fas fa-envelope"></i>
                        <input type="email" placeholder="Email" id="email" name="email" />
                    </div>
                    <div class="input-field">
                        <i class="fas fa-lock"></i>
                        <input type="password" placeholder="Password" id="password" name="password" />
                    </div>
                        <input type="submit" class="btn" value="Sign up" />
                </form>
            </div>
        </div>

        <div class="panels-container">
        <div class="panel left-panel">
          <div class="content">
          <h3>One of us ?</h3>
            <p>
              Log into your account to confirm your orders and proceed to payment!
            </p>
            <button class="btn-register" id="log-in-btn">
              Log in
            </button>
          </div>
          <img src="Images/sign-in.svg" class="image" alt="" />
        </div>
        <div class="panel right-panel">
          <div class="content">
          <h3>New here?</h3>
            <p>
                Register to create an account and make your purchases and deliveries easier!
            </p>
            <button class="btn-register" id="register-btn">
              Register
            </button>

           
          </div>
          <img src="Images/register.svg" class="image" alt="" />
        </div>
      </div>
    </div>

    <script src="JS/app.js"></script>
</div>
</body>
<?php
output_footer()
?>