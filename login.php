<?php
//Start session management 
session_start();

//Get name and adress strings
$name=filter_input(INPUT_POST,'name',FILTER_SANITIZE_STRING);
$password=filter_input(INPUT_POST,'password',FILTER_SANITIZE_STRING);
$email= filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);

//connect to mongoDB and select database
require __DIR__ . '/vendor/autoload.php';
$client = new MongoDB\Client("mongodb://localhost:27017");
$db = $client ->ecommerce;

//create a php array with our search criteria 
$findCriteria= ["name" =>$name ];

//Find all customer that match the criteria
$resultArray = $db ->Customers->find($findCriteria)->toArray();

//check that there is exactly one customer
if(count($resultArray)==0){
    echo 'Customer email not found';
    return;
}

//Get customer and check password
$customer = $resultArray[0];
if($customer['password']!= $password){
    echo '<script language="javascript">';
    echo 'alert("incorrect")';
    echo '</script>';

    header("Location:/ecommerce/register.php");
exit();
    return;
}

//start session for user
$_SESSION['Username']=$name;

//redirect to main page is successful
header("Location:/ecommerce/index.php");
exit();

?>
