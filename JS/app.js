const log_in_btn = document.querySelector("#log-in-btn");
const register_btn = document.querySelector("#register-btn");
const container = document.querySelector(".container");

register_btn.addEventListener("click", () => {
  container.classList.add("register-mode");
});

log_in_btn.addEventListener("click", () => {
  container.classList.remove("register-mode");
});