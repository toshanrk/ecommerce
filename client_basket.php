<!DOCTYPE html>
<html>
    <head>
        <title>Basket Demo</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
        <script src="basket.js"></script>
    </head>

        <!-- PHP loads product information -->        
        <?php

        //connect to mongoDB and select database
        require __DIR__ . '/vendor/autoload.php';
        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client ->ecommerce;
        
        //Find all products
        $products = $db->Products->find();

        //Output results onto page
        echo '<table>';
        echo '<tr><th>ID</th><th>Name</th><th>Description</th><th></th></tr>';
        foreach ($products as $document) {
            echo '<tr>';
            echo '<td>' . $document["_id"] . "</td>";
            echo '<td>' . $document["name"] . "</td>";
            echo '<td>' . $document["description"] . "</td>";
            echo '<td><button onclick=\'addToBasket("' . $document["_id"] . '", "' . $document["name"] . '")\'>';
            echo '<img class="addButtonImg" width=20 src="basket-add-icon.png"></button></td>';
            echo '</tr>';
        }
        echo '</table>';

        ?>
        