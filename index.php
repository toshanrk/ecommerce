<?php
    include 'common.php';
    output_header("Galactech");
    session_start();
?>

<?php
    output_navigation();
?>
<div class="main">
    <div class="computer">
    <a href="shop.php#section1"><h2>Computers & Laptops</h2></a>
    <a href="shop.php#section1"><p>Browse Computers and Laptops <i class="fas fa-long-arrow-alt-right"></i></p></a>
    <div class="computer_description">
        <p> Galactech offers a variety of monitors and laptops ranging from Windows to Macbooks. With just one click you can make your purchase and your order will be delivered to you within 14 working days. Click on the link below to browse through our catalogue of monitors and laptops.</p>
    </div>
        <img src="Images/Alienware_25_monitor__25-inch_1080p_at_240Hz_with__Fast_IPS__panel_1-removebg-preview.png"/>
    </div>
    <div class="mobile-tablet">
    <a href="shop.php#section2"><h2>Mobile & Tablets</h2></a>
    <div class="mobile_description">
        <p>Galactech sells a wide range of smartphones and tablets, including Samsung phones and Wacom Cintiq graphic tablets. You can make a purchase with only one click, and your product will be delivered to you within 14 working days. To browse our selection of mobile phones and tablets, click the link below.</p>
    </div>
    <a href="shop.php#section2"><p>Browse Mobile and Tablets <i class="fas fa-long-arrow-alt-right"></i></p></a>
        <img src="Images/Wacom_Cintiq_graphic_tablet_1.png"/>
    </div>
    <div class="accessories">
    <a href="shop.php#section3"><h2>Accessories</h2></a>
    <div class="accessories_description">
        <p>Galactech carries a variety of accessories, such as earbuds and speakers. You can purchase something with only one click, and it will be delivered to you in 14 working days. Click on the links below to see our assortment of electronic accessories.</p>
    </div>
    <a href="shop.php#section3"><p>Browse Accessories <i class="fas fa-long-arrow-alt-right"></i></p></a>
    <img src="Images/SONY_Headset_1.png"/>
    </div>

    <script>
        var userText = document.getElementById("user");
        console.log(userText);
        var isLoggedIn = <?php echo isset($_SESSION["Username"]);?>;
        if (isLoggedIn) {
            userText.innerHTML = '<?php echo $_SESSION["Username"];?>';
        }

        var signinbtn = document.getElementById("signinbtn");
        signinbtn.innerHTML = "Logout";
        signinbtn.href = "/ecommerce/logout.php";
    </script>
</div>
    
<?php
output_footer()
?>