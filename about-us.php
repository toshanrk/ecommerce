<?php
    include 'common.php';
    output_header("Galactech");
?>

<?php
    output_navigation();
?>
<main>
    <div class="backdrop">
        <h2>ABOUT US</h2>
        <div class="text"><p>Beston Sky Vision Pvt. Ltd. is one of the most professional manufacturers of LED TV and LED Monitors in India. Through dedicated and effective research and development, as well as its belief in continuous improvement, Beston Sky Vision Pvt. Ltd. continues its quest for leadership in the flat panel TV marketplace.<br><br>

The genesis of the company on the industrial landscape took place in 1976. Starting its operation on a small scale, today, Beston Sky Vision Pvt. Ltd. is one of the formidable forces in the electronics industry. Operating development and manufacturing facilities, its main R & D and production base is spread in a 55,000 square feet space. At present, 150 plus strong, experienced and dedicated team, working under the flagship of the company, makes Beston Sky Vision Pvt. Ltd. a market leader in flat-panel LED TV from 24” to 55” screen size.<br><br>

Under the business policy of “Quality first, Customer foremost”, Beston Sky Vision Pvt. Ltd. is committed to providing high-quality products and superior services to satisfy the most demanding customers. A name synonymous with perfection, Beston Sky Vision Pvt. Ltd. has proved its efficacy and efficiency over a period of four decades. Since then the relationship has flourished on a platform of mutual respect and commitment to quality. Various operating divisions, service centers, sales channels and support functions woven in the thread of professionalism and business ethics have made Beston Sky Vision Pvt. Ltd. the most trusted company in the OEM business.</p></div>
</div>
</main>
<?php
output_footer()
?>